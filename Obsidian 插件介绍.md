# Obsidian 插件介绍

## Obsidian git 插件

文档：<https://publish.obsidian.md/git-doc/Start+here>

使用 git 备份您的 Obsidian.md 库

Obsidian Git:Add file to gitignore 添加文件到 gitignore  
Obsidian Git:CAUTION:Delete repository 注意: 删除存储库  
Obsidian Git:CAUTION:Discard all changes 注意: 放弃所有更改  
Obsidian Git:Clone an existing remote repo 克隆一个现有的远程仓库  
Obsidian Git:Commit all changes 提交所有更改  
Obsidian Git:Commit all changes with specific message 提交带有特定消息的所有更改  
Obsidian Git:Commit staged 提交阶段  
Obsidian Git:Commit staged with specific message 使用特定的消息进行阶段性提交  
Obsidian Git:Create backup 创建备份  
Obsidian Git:Create backup and close 创建备份并关闭  
Obsidian Git:Create backup with specific message 创建带有特定消息的备份  
Obsidian Git:Create new branch 创建新分支  
Obsidian Git:Delete branch 删除分支  
Obsidian Git:Edit .gitignore 编辑 .gitignore  
Obsidian Git:Edit remotes 编辑远程  
Obsidian Git:Initialize a new repo 初始化一个新的仓库  
Obsidian Git:List changed files 列出更改的文件  
Obsidian Git:Open diff view 打开 diff 视图  
Obsidian Git:Open file history on GitHub 在 GitHub 上打开文件历史记录  
Obsidian Git:Open file on GitHub 在 GitHub 上打开文件  
Obsidian Git:Open history view : 打开历史视图  
Obsidian Git:Open source control view 开源控制视图  
Obsidian Git:Pull 拉  
Obsidian Git:Push 推  
Obsidian Git:Remove remote 移除远程  
Obsidian Git:Stage current file 阶段当前文件  
Obsidian Git:Switch branch 切换分支  
Obsidian Git:Switch to remote branch 切换到远程分支  
Obsidian Git:Toggle line author information 切换行作者信息  
Obsidian Git:Unstage current file 卸载当前文件

Miscellaneous  
杂项

Automatically refresh Source Control View on file changes  
在文件更改时自动刷新源代码控制视图

On slower machines this may cause lags. If so,just disable this option  
在较慢的机器上，这可能会导致延迟。如果是，请禁用此选项

Source Control View refresh interval  
源控制视图刷新间隔

Milliseconds to wait after file change before refreshing the Source Control View  
文件更改后，在刷新源控制视图之前等待的毫秒数

Disable notifications  
禁用通知

Disable notifications for git operations to minimize distraction(refer to status bar for updates). Errors are still shown as  
禁用 git 操作的通知以减少干扰 (请参考状态栏中的更新)。错误仍然显示为

notifications even if you enable this setting  
通知，即使您启用了此设置

Show status bar  
显示状态栏

Obsidian must be restarted for the changes to take affect  
Obsidian 必须重新启动以使更改生效

Show stage/unstage button in file menu  
在文件菜单中显示舞台/舞台按钮

Show branch status bar  
显示分支状态栏

Obsidian must be restarted for the changes to take affect  
Obsidian 必须重新启动以使更改生效

Show changes files count in status bar  
在状态栏中显示更改文件计数

Commit author  
提交作者

Advanced  
先进的  

Update submodules  
更新子模块

"Create backup"and "pull"takes care of submodules. Missing features:Conflicted files,count of pulled/pushed/committed files.  
“创建备份”和“拉”负责子模块。缺少的特性: 冲突的文件，拉/推/提交文件的计数。  

ng branch needs to be set for each submodule  
每个子模块需要设置一个分支  

Custom Git binary path  
自定义 Git 二进制路径  

Additional environment variables  
其他环境变量

Use each line for a new environment variable in the format KEY=VALUE  
以 KEY=VALUE 的格式将每行用作一个新的环境变量  

Additional PATH environment variable paths  
附加的 PATH 环境变量路径  

Use each line for one path  
每一行代表一个路径  

Reload with new environment variables  
用新的环境变量重新加载  

Removing previously added environment variables will not take effect until Obsidian is restarted.  
移除之前添加的环境变量将不会生效，直到黑曜石重新启动。  

Custom base path (Git repository path)  
自定义基路径 (Git 存储库路径)  

Sets the relative path to the vault from which the Git binary should be executed. Mostly used to set the  
设置要执行 Git 二进制文件的仓库的相对路径。主要用来设置 目录/ directory-with-g 

path to the Git repository,which is only required if the Git repository is below the vault root directory.  
路径到 Git 存储库，只有当 Git 存储库位于保险库根目录之下时才需要。  

Custom Git directory path(Instead of '.git')  
自定义 Git 目录路径 (而不是 '.git')  

Requires restart of Obsidian to take effect. Use "\instead of "/on Windows.  
需要重启黑曜石才能生效。在 Windows 上使用“\”代替“/”。







  



